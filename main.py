#@author Wojciech Wolny
#Program sprawdzający czy dany tekst da się
#zapisać w podanej liczbie kroków w danej gramatyce
#Jeśli tak wypluwa True, w przeciwnym wypadku False
#Gramatyka jest zapisana w formie tablicy dwuwymiarowej
#pod indeksem 0 znajduje się to co będzie zamieniane
#pod kolejnymi indeksami znajduja możliwości zamiany
#Struktura danych:
#1) gramatyka: mapa, na pierwszym miejscu każdego wiersza to co bedzie zamieniane
# ... na kolejnych na co można zamienić
#2) tekst: lista par zawierających słowo końcowe i liczbę kroków

import sys
import string


def main():
    if len(sys.argv) == 3:
        nazwa_gram = sys.argv[1]
        tekst = sys.argv[2]
    else:
        return False
    try:
        grammar = open(nazwa_gram)
        vocab = open(tekst)
    except OSError:
        print("Wrong files")
        return False
    grammarRules = checkGrammar(grammar)
    if grammarRules == False:
        return False
    fullText = checkVocab(vocab)
    if fullText == False:
        return False
    checkIfTextIsCorrect(grammarRules, fullText)
    return True

#Sprawdzam czy plik jest odpowiednio zapisany i zapisuje do struktury
def checkGrammar(gramm):
    grammarRules = list()
    for row in gramm:
        newRow = list()
        newRow.clear()
        temp = row.split('->')
        if len(temp) != 2:
            return False
        if len(temp[0]) != 1:
            return False
        if temp[0] in string.ascii_uppercase:
            newRow.append(row[0])
        else:
            return False
        rules = temp[1].split('|')
        if len(rules) < 1:
            return False
        for i in range(len(rules)):
            tmpWord = rules[i].replace('\n','')
            if len(tmpWord) == 0:
                return False
            for letter in tmpWord:
                if not letter in string.ascii_letters and not letter == '_':
                    return False
            extWord = tmpWord.replace('_','')
            newRow.append(extWord)
        tmpRow = newRow[:]
        grammarRules.append(tmpRow)
    for i in range(len(grammarRules)):
        if grammarRules[i][0] == 'S':
            return grammarRules
    return False


#Sprawdzam czy plik jest odpowiednio zapisany i zapisuje do struktury
def checkVocab(vocab):
    fulltext = list()
    for line in vocab:
        tmp = line.replace("\n", "")
        newRow = tmp.split(",")
        if len(newRow) != 2:
            return False
        for letter in newRow[0]:
            if letter not in string.ascii_lowercase:
                return False
        if len(newRow[1]) < 1:
            return False
        for digit in newRow[1]:
            if digit not in string.digits:
                return False
        tempRow = newRow[:]
        fulltext.append(tempRow)
    return fulltext


#Sprawdzam czy da się zrobić program w danej liczbie krokow
def checkIfTextIsCorrect(gramStructure, text):
    for i in range(len(text)):
        steps = int(text[i][1])
        if checkNextStep('S', text[i][0], steps, gramStructure):
            printLine(text[i][0], text[i][1], True)
        else:
            printLine(text[i][0], text[i][1], False)

#Rekurencyjna funkjca sprawdzajaca czy da sie zrobic slowo korzystajac z kolejnych zasad gramatycznych
def checkNextStep(startWord, finalWord, steps, grammar):
    if steps <= 0:
        return False
    if len(startWord) > len(finalWord)+10:
        return False
    if startWord == finalWord:
        return True
    tmpWord = ""
    for letterId in range(len(startWord)):
        if startWord[letterId] in string.ascii_uppercase:
            for ruleId in range(len(grammar)):
                if startWord[letterId] == grammar[ruleId][0]:
                    for changeId in range(1, len(grammar[ruleId])):
                        if letterId == len(startWord)-1:
                            tmpWord = startWord[:letterId]+grammar[ruleId][changeId]
                        else:
                            tmpWord = startWord[:letterId]+grammar[ruleId][changeId] + startWord[letterId+1:]
                        if checkNextStep(tmpWord, finalWord, steps-1, grammar):
                            return True
    return False

#wypisuje zdania
def printLine(word, restriction, truth):
    if truth:
        print(word+','+restriction+': True\n')
    else:
        print(word+','+restriction+': False\n')


main()
